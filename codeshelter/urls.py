"""codeshelter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.contrib.staticfiles import views
from django.urls import include, path, re_path

urlpatterns = [
    path("entrary/", include("loginas.urls")),
    path("entrary/", admin.site.urls),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("", include("main.urls")),
    path("", include("social_django.urls", namespace="social")),
]

if settings.DEBUG:
    urlpatterns += [re_path(r"^static/(?P<path>.*)$", views.serve)]
