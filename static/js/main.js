var BurgerButton = (function() {

  var element = null;
  let content = null;

  init = function() {

    element = document.querySelector('[data-burger]');
    content = document.querySelector('[data-burger-content]');

    if(element == null)
      return;

    element.addEventListener('click', () => {

      if(!isOpen()) {
        element.dataset['open'] = true;
        content.dataset['open'] = true;
      }
      else {
        close();
      }
    });

    window.addEventListener('click', (event) => {

      if(event.target == element)
        return;

      close();
    });

    window.addEventListener('keydown', (event) => {

      if(event.keyCode != 27)
        return;

      close();

    }, false);
  }

  isOpen = function() {
    return (element.dataset['open'] != undefined);
  }

  close = function() {

    if(!isOpen())
      return;

    delete element.dataset['open'];
    delete content.dataset['open'];
  }

  return {
      'initialize': init
  }

})();

document.addEventListener('DOMContentLoaded', () => {

    BurgerButton.initialize();
});
