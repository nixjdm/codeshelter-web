import hashlib
import hmac
import json

from django.conf import settings
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .models import GithubInstallation, Project


def remove_github_repository(repository: dict, payload: dict):
    """Remove a Github repository from the database."""
    Project.objects.filter(
        name=repository["name"],
        owner_username=repository["full_name"].split("/")[0],
        github_installation__installation_id=payload["installation"]["id"],
    ).delete()


def event_installation_repositories(payload: dict) -> dict:
    print("Repos changed.")
    for repository in payload["repositories_added"]:
        Project.add_github(repository, payload)
    for repository in payload["repositories_removed"]:
        remove_github_repository(repository, payload)
    return {}


def event_installation(payload: dict) -> dict:
    if payload["action"] == "created":
        print("New installation.")
        for repository in payload["repositories"]:
            Project.add_github(repository, payload)
    elif payload["action"] == "deleted":
        GithubInstallation.objects.filter(installation_id=payload["installation"]["id"]).delete()
    return {}


@csrf_exempt
def github_webhook(request):
    signature = request.META.get("HTTP_X_HUB_SIGNATURE", "")[5:]
    if not hmac.compare_digest(
        hmac.new(
            key=settings.GITHUB_WEBHOOK_SECRET.encode("utf8"), msg=request.body, digestmod=hashlib.sha1
        ).hexdigest(),
        signature,
    ):
        return JsonResponse({"error": "Signature verification failed."}, status=422)

    try:
        payload = json.loads(request.body)
    except json.JSONDecodeError:
        return JsonResponse({"error": "Could not decode JSON body."}, status=422)

    event_type = request.META.get("HTTP_X_GITHUB_EVENT", "")

    # Event dispatcher.
    response = {"installation": event_installation, "installation_repositories": event_installation_repositories}.get(
        event_type, lambda payload: {}
    )(payload)
    return JsonResponse(response)
