import gitlab
from django.conf import settings
from django.core.management.base import BaseCommand
from main.exceptions import GithubUnauthorizedException
from main.models import Project


class Command(BaseCommand):
    help = "Update all projects."

    def handle(self, *args, **options):
        print("Fetching projects from GitLab...")
        gl = gitlab.Gitlab("https://www.gitlab.com/", private_token=settings.GITLAB_API_TOKEN)

        for project in gl.groups.get(settings.GITLAB_GROUP_ID).projects.list():
            Project.add_gitlab(project)

        print("Updating projects...")
        for project in Project.objects.all():
            print(f"Updating {project.short_name}...")
            try:
                project.update()
            except GithubUnauthorizedException:
                print("Could not update project.")
                continue

            # Always add the shelter mark when updating.
            project.add_shelter_mark()
        print("Done.")
