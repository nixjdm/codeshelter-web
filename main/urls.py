"""The main application's URLs."""

from django.urls import path

from . import feeds, github_event_views, views

urlpatterns = [
    path("", views.index, name="index"),
    path("dashboard/", views.home, name="home"),
    path("faq/", views.faq, name="faq"),
    path("membership/", views.maintainer_application, name="maintainer-application"),
    path("account/", views.account, name="account"),
    path("projects/", views.projects, name="projects"),
    path("projects/feed/", feeds.LatestProjectsFeed(), name="projects-feed"),
    path("projects/add/", views.project_add, name="project-add"),
    path("projects/<project_id>/", views.project, name="project"),
    path("projects/<project_id>/invite/", views.maintainer_add, name="maintainer-add"),
    path("maintainers/", views.maintainers, name="maintainers"),
    path("maintainers/feed/", feeds.LatestMaintainersFeed(), name="maintainers-feed"),
    path("contributors/", views.contributors, name="contributors"),
    path("contributors/feed/", feeds.LatestContributorsFeed(), name="contributors-feed"),
    path("github/webhook/", github_event_views.github_webhook, name="github-webhook"),
]
