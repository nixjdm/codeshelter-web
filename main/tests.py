import os

from django.conf import settings
from django.template import Template
from django.test import TestCase
from django.urls import reverse

from .models import Project, User


class SmokeTests(TestCase):
    def setUp(self):
        User.objects.create(username="maintainer1", role="MA")
        User.objects.create(username="contributor1", role="CO")
        Project.objects.create(name="testproject", owner_username="someowner")

    def test_public_urls(self):
        urls = [
            "index",
            "faq",
            "contributors",
            "contributors-feed",
            "maintainers",
            "maintainers-feed",
            "projects",
            "project-add",
            "projects-feed",
            "maintainer-application",
        ]
        for url in urls:
            response = self.client.get(reverse(url))
            self.assertEqual(response.status_code, 200)

    def test_compile_templates(self):
        for template_dir in settings.TEMPLATES[0]["DIRS"]:
            for basepath, dirs, filenames in os.walk(template_dir):
                for filename in filenames:
                    path = os.path.join(basepath, filename)
                    with open(path, "r") as f:
                        # This will fail if the template cannot compile
                        Template(f.read())
