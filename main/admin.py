from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import GithubInstallation, Project, User


@admin.register(User)
class MyUserAdmin(UserAdmin):
    change_form_template = "loginas/change_form.html"
    fieldsets = (
        (_("Credentials"), {"fields": ("username", "email", "password")}),
        (_("Details"), {"fields": ("first_name", "last_name", "avatar")}),
        (_("Permissions"), {"fields": ("role", "is_active", "is_staff", "is_superuser", "user_permissions", "groups")}),
        (_("Important dates"), {"fields": ("last_login", "date_joined", "maintainer_date")}),
        (_("Various"), {"fields": ("github_repositories", "receive_notifications")}),
    )
    list_display = ("email", "username", "is_staff", "date_joined", "role", "receive_notifications")
    list_filter = ("role",)
    search_fields = ("email",)
    ordering = ["email"]


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ["owner_username", "name", "github_installation", "stars", "created", "language"]
    search_fields = ["name", "owner_username"]
    list_filter = ("created",)
    ordering = ["-created"]


@admin.register(GithubInstallation)
class GithubInstallationAdmin(admin.ModelAdmin):
    list_display = ["owner_username", "installation_id"]
