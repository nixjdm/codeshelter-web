from django.contrib.syndication.views import Feed
from django.urls import reverse

from .models import Project, User


class LatestProjectsFeed(Feed):
    title = "New Code Shelter projects"
    link = "/projects/"
    description = "A feed of projects added to Code Shelter."

    def items(self):
        return Project.objects.order_by("-created")[:10]

    def item_title(self, item):
        return item.short_name

    def item_description(self, item):
        return f"{item.description}\n\n★ {item.stars} - {item.repo_url}"

    def item_link(self, item):
        return reverse("projects")

    def item_pubdate(self, item):
        return item.created

    def item_guid(self, item):
        return item.id


class LatestMaintainersFeed(Feed):
    title = "New Code Shelter maintainers"
    link = "/maintainers/"
    description = "A feed of the latest Code Shelter maintainers."

    def items(self):
        return User.objects.maintainers().order_by("-maintainer_date")[:10]

    def item_title(self, item):
        return item.get_full_name()

    def item_description(self, item):
        return item.github_url

    def item_link(self, item):
        return item.github_url or "/"

    def item_pubdate(self, item):
        return item.maintainer_date


class LatestContributorsFeed(Feed):
    title = "New Code Shelter contributors"
    link = "/contributors/"
    description = "A feed of the latest Code Shelter contributors."

    def items(self):
        return User.objects.contributors().order_by("-date_joined")[:10]

    def item_title(self, item):
        return item.get_full_name()

    def item_description(self, item):
        return item.github_url

    def item_link(self, item):
        return item.github_url or "/"

    def item_pubdate(self, item):
        return item.date_joined
