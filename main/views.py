from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.http import require_http_methods

from .models import Project, User


def maintainer_application(request):
    return render(request, "maintainer_application.html")


def project_add(request):
    return render(request, "project_add.html")


def faq(request):
    return render(request, "faq.html")


def index(request):
    return render(
        request,
        "index.html",
        {
            "projects": Project.objects.all()[:10],
            "maintainers": User.objects.maintainers()[:35],
            "contributors": User.objects.contributors()[:35],
        },
    )


@user_passes_test(lambda u: u.is_authenticated)
def home(request):
    if request.user.is_maintainer:
        # Maintainers maintain projects for which they are maintainers, and all GitLab projects.
        your_projects = Project.objects.filter(Q(maintainers=request.user) | Q(host="GL")).order_by("name")
        other_projects = Project.objects.exclude(maintainers=request.user).exclude(host="GL").order_by_need()
    else:
        # Contributors don't formally maintain any projects, unfortunately.
        your_projects = []
        other_projects = Project.objects.all()
    return render(request, "home.html", {"your_projects": your_projects, "other_projects": other_projects})


@user_passes_test(lambda u: u.is_authenticated and (u.is_maintainer or u.is_contributor))
def project(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    return render(request, "home.html", {"other_projects": [project]})


@user_passes_test(lambda u: u.is_authenticated and (u.is_maintainer or u.is_contributor))
def account(request):
    class AccountForm(forms.Form):
        receive_notifications = forms.BooleanField(
            required=False, help_text="Receive email digest for added projects, at most once a day."
        )

    initial_data = {"receive_notifications": request.user.receive_notifications}
    if request.method == "POST":
        form = AccountForm(request.POST, initial=initial_data)
        if form.is_valid():
            request.user.receive_notifications = form.cleaned_data["receive_notifications"]
            request.user.save()

            messages.success(request, "Your preferences have been updated.")
            return redirect(account)
    else:
        form = AccountForm(initial=initial_data)
    return render(request, "account.html", {"form": form})


def projects(request):
    return render(request, "projects.html", {"projects": Project.objects.all()})


def contributors(request):
    return render(request, "contributors.html", {"contributors": User.objects.contributors()})


def maintainers(request):
    return render(request, "maintainers.html", {"maintainers": User.objects.maintainers()})


@user_passes_test(lambda u: u.is_authenticated and u.is_maintainer)
@require_http_methods(["POST"])
def maintainer_add(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    try:
        project.gh_invite_collaborator(request.user)
    except AssertionError as e:
        messages.error(request, e.args[0])
    else:
        messages.success(request, "You have been invited to the repository, please check your email.")
    return redirect("index")
